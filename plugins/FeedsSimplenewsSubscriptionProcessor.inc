<?php

/**
 * @file
 * FeedsUserProcessor class.
 */

/**
 * Feeds processor plugin. Create users from feed items.
 */
class FeedsSimplenewsSubscriptionProcessor extends FeedsProcessor {
  /**
   * Define entity type.
   */
  public function entityType() {
    return 'simplenews_subscription';
  }

  /**
   * Implements parent::entityInfo().
   */
  protected function entityInfo() {
    $info = parent::entityInfo();
    $info['label plural'] = t('Simplenews subscriptions');
    return $info;
  }

  /**
   * Creates a new simplenews subscription in memory and returns it.
   */
  protected function newEntity(FeedsSource $source) {
    $subscriber = new stdClass();
    $subscriber->uid = 0;
    $subscriber->activated = 1;
    $subscriber->language = '';
    return $subscriber;
  }

  /**
   * Loads an existing subscription.
   */
  protected function entityLoad(FeedsSource $source, $snid) {
    return simplenews_subscriber_load($snid);
  }

  /**
   * Validates a subscription.
   */
  protected function entityValidate($subscriber) {
    if (empty($subscriber->mail) && valid_email_address($subscriber->mail)) {
      throw new FeedsValidationException(t('E-mail is missing or invalid.'));
    }

    if (!isset($subscriber->tids)) {
      throw new FeedsValidationException(t('Newsletter category is missing or invalid.'));
    }
  }

  /**
   * Save a subscription.
   */
  protected function entitySave($subscriber) {
    simplenews_subscriber_save($subscriber);

    foreach ($subscriber->tids as $tid) {
      simplenews_subscribe_user($subscriber->mail, $tid, FALSE, 'Feeds: ' . $this->id);
    }

    feeds_item_info_save($subscriber, $subscriber->snid);
  }

  /**
   * Delete multiple subscriptions.
   */
  protected function entityDeleteMultiple($snids) {
    simplenews_subscription_delete_multiple($snids);
  }

  /**
   * Override setTargetElement to operate on a target item that is a simplenews subscription.
   */
  public function setTargetElement(FeedsSource $source, $target_subscription, $target_element, $value) {
    if ($target_element == 'tids') {
      $terms = explode(',', $value);
      $simplenews_categories = array_flip(array_map('strtolower', simplenews_category_list()));

      foreach ($terms as $term) {
        $term = check_plain(trim(strtolower($term)));

        if (is_numeric($tid = $simplenews_categories[$term])) {
          if (!in_array($tid, $target_subscription->tids)) {
            $target_subscription->tids[] = $tid;
          }
        }
      }
    }
    else {
      parent::setTargetElement($source, $target_subscription, $target_element, $value);
    }
  }

  /**
   * Return available mapping targets.
   */
  public function getMappingTargets() {
    $targets = parent::getMappingTargets();
    $targets += array(
      'activated' => array(
        'name' => t('Subscription status'),
        'description' => t('Boolean indicating the status of the subscription.'),
       ),
      'mail' => array(
        'name' => t('Email address'),
        'description' => t('Boolean indicating the status of the subscription.'),
        'optional_unique' => TRUE,
       ),
       'tids' => array(
        'name' => t('Subscription category'),
        'description' => t('The category (simplenews_category.tid) the subscriber is subscribed to.'),
       ),
      'language' => array(
        'name' => t('Language'),
        'description' => t('Boolean indicating the status of the subscription.'),
       ),
      'timestamp' => array(
        'name' => t('Timestamp'),
        'description' => t('UNIX timestamp of when the user is (un)subscribed.'),
       ),
       'changes' => array(
        'name' => t('Changes'),
        'description' => t('Contains the requested subscription changes.'),
       ),
       'created' => array(
        'name' => t('Created'),
        'description' => t('UNIX timestamp of when the subscription record was added.'),
       ),
    );

    // Let other modules expose mapping targets.
    self::loadMappers();
    $entity_type = $this->entityType();
    $bundle = $this->entityType();
    drupal_alter('feeds_processor_targets', $targets, $entity_type, $bundle);

    return $targets;
  }

  /**
   * Get id of an existing feed item term if available.
   */
  protected function existingEntityId(FeedsSource $source, FeedsParserResult $result) {
    if ($snid = parent::existingEntityId($source, $result)) {
      return $snid;
    }

    // Iterate through all unique targets and try to find a user for the
    // target's value.
    foreach ($this->uniqueTargets($source, $result) as $target => $value) {
      switch ($target) {
        case 'mail':
          $snid = db_query("SELECT snid FROM {simplenews_subscriber} WHERE mail = :mail", array(':mail' => $value))->fetchField();
          break;
      }
      if ($snid) {
        // Return with the first snid found.
        return $snid;
      }
    }
    return 0;
  }
}
